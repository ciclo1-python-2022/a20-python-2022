
# Programa a Ejecutar del Módulo de la Función Ejemplo Establecer Valores por Defecto...

# ----------------------------
# Importa el Módulo => Hacer el llamado al Archivo que tiene la función(es)
import u2_moduloValorPorDefectoFun

# ----------------------------
# Ejecutar el Programa...
print(f"Retorno de la función: {u2_moduloValorPorDefectoFun.valor()}\n")
print(f"Retorno de la función: {u2_moduloValorPorDefectoFun.valor(10)}\n")
c = "Tripulantes "
print(f"Retorno de la función: {u2_moduloValorPorDefectoFun.valor(c)}\n")