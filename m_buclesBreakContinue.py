
# Explicación de Break y Continue en Bucles


# ------------------------ Break -------------------------------

print("\nAntes de iniciar el for...\n")
for i in range(10):
    print("i = ", i)
    # break
    if i == 3:
        print("Antes del break...")
        break
        print("Después del break...")
    print("Después del condicional...")

print("\nSalió del for...")


print("\nAntes de iniciar el while...\n")

cont = 0
while cont < 10:
    print("cont = ", cont)
    # break
    if cont == 3:
        print("Antes del break...")
        break
        print("Después del break...")
    print("Después del condicional...")
    cont += 1

print("\nSalió del while...")






# ------------------------ Continue -------------------------------

print("\nAntes de iniciar el for...\n")
for i in range(10):
    print("i = ", i)
    # break
    if i == 3:
        print("Antes del continue...")
        continue
        print("Después del continue...")
    print("Después del condicional 1...")
    if i % 2 == 0:
        print("El valor de de i =", i, "es Par")
    else:
        print("El valor de de i =", i, "es Impar")
    print("Después del condicional 2...")

print("\nSalió del for...")



print("\nAntes de iniciar el while...\n")

cont = 0
while cont < 10:
    cont += 1
    print("cont = ", cont)
    # break
    if cont == 3:
        print("Antes del continue...")
        continue
        print("Después del continue...")
    print("Después del condicional...")
    # cont += 1

print("\nSalió del while...")


# ------------------------------------------
print("\nOmite Multiplos de 4 hasta 20\n")
sumatoria = 0
for i in range(21):
    if i % 4 == 0:
        print("Valor i =", i, "es múltiplo de 4...")
        continue
    print("i = ", i)
    sumatoria += i
    print("Sumatoria =", sumatoria)

print("\nTerminó el for...\n")