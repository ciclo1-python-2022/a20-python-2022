

# Programa para Generar Contraseñas Seguras

# importar el módulo
import x_moduloGenContSeg

psw = x_moduloGenContSeg.generarContrasegnaSegura()

print(f"\nContraseña Generada: {psw}")
print(f"Tamaño Contraseña: {len(psw)}\n")