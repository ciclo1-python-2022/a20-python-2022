
# Programa para comparar el precio de 2 productos

# Entrada(s)
print("------")

# precioProd1 = 5
precioProd1 = float(input("Digite el precio del producto 1: "))
# precioProd2 = 6
precioProd2 = float(input("Digite el precio del producto 2: "))

# Transformación - Salida(s)
print("------")
print("Inicia if precioProd1 < precioProd2")
if precioProd1 < precioProd2:
    print("La respuesta es True del primer condicional.")
    print("El precio del producto 1 es menor que el precio del producto 2.")
elif precioProd1 == precioProd2:
    print("La respuesta es True condicional else if.")
    print("El precio del producto 1 igual al precio del producto 2.")
else:
# elif precioProd2 < precioProd1:
    print("La respuesta es False del condicional.")
    print("El precio del producto 2 es menor que el precio del producto 1.")
# else:
    print("Otro caso...")

print("Fin if precioProd1 < precioProd2")
print("------")

