
# Módulo para generar contraseñas seguras
# Incluir Mayúsculas, minúsculas, números, símbolos (caracteres especiales)
# Longitud mínima 8 caracteres.

# Utilizar el Módulo Random
# import random
from random import randint, choice


# Declarar o definir la función
def generarContrasegnaSegura():
    # Entradas
    # ls = ["A", "B", "C"...]
    mayusculas = "ABCDEFGHIJALMNÑOPQRSTUVWXYZ"
    # minusculas = mayusculas.lower().split() # minusculas y en una lista
    minusculas = list(mayusculas.lower()) # minusculas y en una lista
    # print(f"minusculas: {minusculas}")
    # mayusculas = mayusculas.split() # mayusculas en una lista
    mayusculas = list(mayusculas) # mayusculas en una lista
    # print(f"mayusculas: {mayusculas}")
    # minusculas = minusculas.split() # minusculas en una lista
    # numeros = "1234567890"
    # numeros = numeros.split()
    # numeros = "1234567890".split() # números a una lista
    numeros = list("1234567890") # números a una lista
    # print(f"numeros: {numeros}")
    # simbolos = ".,+*/@#$%&=-_:;¿?!¡[]{}()".split() # símbolos a una lista
    simbolos = list(".,+*/@#$%&=-_") # símbolos a una lista
    # print(f"simbolos: {simbolos}")

    caracteresUtilizar = numeros + mayusculas + numeros + minusculas + numeros + simbolos
    # print(f"caracteresUtilizar: {caracteresUtilizar}")

    # contrasegna = []   # partir de una lista vacía
    contrasegna = list() # partir de una lista vacía

    longContr = randint(10, 20)  # Longitud de la contraseña a generar
    # print(f"longContr: {longContr}")

    # Transformación
    for i in range(longContr):
        caractAleat = choice(caracteresUtilizar)  # selecciona un caracter aleatoria de la lista caracteresUtilizar
        # print(f"caractAleat: {caractAleat}")
        contrasegna.append(caractAleat)  # agrega el caracter aleatorio seleccionado a la lista que se está generando como contraseña
        # contrasegna.append(choice(caracteresUtilizar))
        # print(f"contrasegna: {contrasegna}")

    # contrasegna = "".join(contrasegna)
    contrasegna = str().join(contrasegna)
    # print(f"contrasegna: {contrasegna}")

    # Salida
    return contrasegna





    