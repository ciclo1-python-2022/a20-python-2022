
# Programa para Clasificar Recipientes Cilíndricos

# Una empresa que fabrica recipientes cilíndricos, desea automatizar 
# el proceso de clasificación teniendo en cuenta el volumen del 
# recipiente (utilizando lista de datos). 

# La empresa clasifica los productos respecto a la Tabla 1 adjunta. 
# Adicionalmente, empaca  las “Copas” en cajas de 10 unidades, 
# los “Vasos” en cajas de 6 unidades, los “Frascos” en cajas de 40 unidades, 
# as “Botellas peq” en cajas de 30 unidades, las “Botellas med” en 
# cajas de 20 unidades, las “Botellas gran” en cajas de 10 unidades 
# y lo demás “Descartado” en cajas de 100 unidades.

# Programar un sistema que mediante una lista de datos con el volumen 
# de los recipientes, informe su respectiva clasificación, cantidad 
# y número de cajas disponibles.


# ---------------------------------------
# Programa a Ejecutar

# Importar módulos
from random import randint, uniform
from z_moduloClasifRecip import volRecipCilindrico, clasificacionRecipientes

# Entradas => diámetro y altura del cilindro para obtener el volumen
# la cantidad de recipientes a producir
cantRecip = randint(500, 10000)
# cantRecip = 100
print(f"\nCantidad Recipientes a Clasificar: {cantRecip}\n")
# volRecip = []
listaVolRecip = list()

for i in range(cantRecip):
    alto = uniform(3, 30)
    diametro = uniform(3, 10)
    volumen = volRecipCilindrico(diametro, alto)
    listaVolRecip.append(volumen)
    # listaVolRecip.append(volRecipCilindrico(diametro, alto))
print(f"\nLista con el volumen de los {cantRecip} Recipientes:")
print(listaVolRecip, "\n")

# Transformación
# Clasificación de la lista de recipientes
clasiRecip = clasificacionRecipientes(listaVolRecip)

# Salida
print("\nEl Resultado de la Casificación de Recipientes es:")
print(clasiRecip, "\n")
