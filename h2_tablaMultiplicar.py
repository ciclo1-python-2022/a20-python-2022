
# Programa para generar tablas de multiplicación

tablaMultip = 1
menu = """
#########################################################

Para finalizar el programa, digite un valor negativo o cero(0)

Digite el número hasta el cual desea generar las tablas de multiplicar: """

while tablaMultip > 0:
    # pass
    tablaMultip = int(input(menu))
    # tablaMultip = int(input("Digite un número"))
    # print("\n###### Tabla de Multiplicar del", tablaMultip, "######\n")
    for multiplicando in range(1, tablaMultip + 1):
        print("\n###### Tabla de Multiplicar del", multiplicando, "######\n")
        for multiplicador in range(1, 11):
            print(multiplicando, "x", multiplicador, "=", multiplicando * multiplicador)

print("\n#########################################################")
print("\nFin del Programa\n")