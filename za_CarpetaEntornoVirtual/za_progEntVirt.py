
# Entornos Virtuales en Python

# EntVirt en Python es un directorio (carpeta) que contiene una 
# versión (instalación) de Python y donde podemos adicionar 
# librerías, frameworks, etc. específicas.

# Los Framewor se pueden componer de Librerías
# Las Librerías se componen de Módulos
# Los Módulos se compones de Funciones - Métodos

# Crear Entorno Virtual => Crear una carpeta za_progEntVirt
# Ubircarme en la carpeta a crear el entorno virtual (desde consoloa - terminal)
# Ejecutar comando en consola (terminal - cmd) => python3 -m venv carpEntVirt
# Tutorial en => https://docs.python.org/es/3/tutorial/venv.html
# se creó la cartpeta (directorio) carpEntVirt con las respectivas librerías
# para trabajar en ese entorno virtual o proyecto

# Después debemos Activar el entorno virtual
# Windows => carpEntVirt\Scripts\activate.bat
# MacOS/Linux => source carpEntVirt/bin/activate
# al activarlo, muestra en consola el directorio 
# (carpEntVirt) al inicio de la línea de comandos

# Una vez activado el entorno virtual, podemos empezar a trabajar 
# en los archivos/script.py e instalar librerías en el entorno virtual


# Comando => pip list
# Muestra los paquetes que tengo instalados
# Ejemplo:
# Package    Version
# ---------- -------
# pip        22.1.1
# setuptools 58.1.0

# ########### Importante ############
# Tener cuidado dónde ejecuto el programa
# en relación al entorno virtual


x = 3
print(f"\nEl valor de x = {x}\n")


# -------------------------------
# Ciencia de Datos => Librería más utilizada es Pandas
# Página Oficial => https://pandas.pydata.org/docs/

# Pandas es una Librería (incluye módulos), incluye los dataframe (matriz o tablas)
# Los dataframe me permiten manipular los datos con mayor facilidad.
# Ejecuto el comando => pip install pandas
# Al ejecutar nuevamente => pip list
# Package         Version
# --------------- -------
# numpy           1.22.4
# pandas          1.4.2
# pip             22.1.1
# python-dateutil 2.8.2
# pytz            2022.1
# setuptools      58.1.0
# six             1.16.0


# Ahora a utilizar la librería de pandas
# Por convención suele utilizarse el alias pd
import pandas as pd

variable = pd.read_csv("za_archNotasEst.csv")

print("\nEl Archivo 'za_archNotasEst.csv' Contiene:")
print(variable, "\n")

print(f"Tipo dato: {type(variable)}")


variable["promNot"] = (variable["nt1"] + variable["nt2"] + variable["nt3"] + variable["nt4"] + variable["nt5"]) / 5
print("El DataFrame con el promedio de las 5 notas es:")
print(variable, "\n")

print("El DataFrame con promedio > 4.0 notas es:")
print(variable[variable["promNot"] > 4], "\n")

