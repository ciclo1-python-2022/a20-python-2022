
# -------------------------------------------------
# Declarar o Definir la Función
# Parámetro o argumento: Nombre de la variable que guarda el valor que recibe/envía la función.
def saludo(dato):
    print("\nHola Tripulantes")
    print("Programa para ejecutar la opción", dato)
    print("Termina la ejecución de la opción", dato)
    print("Vuelva pronto...\n")
    return None


# -------------------------------------------------
# Programa a ejecutar...

opcion = input("\nSeleccione una opción (1, 2, 3, 4, 5, 6): ")

if opcion == "1":
    saludo(opcion)
elif opcion == "2":
    saludo(opcion)
elif opcion == "3":
    saludo(opcion)
elif opcion == "4":
    saludo(opcion)
elif opcion == "5":
    saludo(opcion)
elif opcion == "6":
    saludo(opcion)
    saludo(opcion * 10)
    saludo(7)

else:
    print("\nSeleccione una opción válida...\n")



# -------------------------------------------------
# Programa a ejecutar...

# opcion = input("\nSeleccione una opción (1, 2, 3, 4, 5, 6): ")

# if opcion == "1":
#     print("\nHola Tripulantes")
#     print("Programa para ejecutar la opción", opcion)
#     print("Termina la ejecución de la opción", opcion)
#     print("Vuelva pronto...\n")
# elif opcion == "2":
#     print("\nHola Tripulantes")
#     print("Programa para ejecutar la opción", opcion)
#     print("Termina la ejecución de la opción", opcion)
#     print("Vuelva pronto...\n")
# elif opcion == "3":
#     print("\nHola Tripulantes")
#     print("Programa para ejecutar la opción", opcion)
#     print("Termina la ejecución de la opción", opcion)
#     print("Vuelva pronto...\n")
# elif opcion == "4":
#     print("\nHola Tripulantes")
#     print("Programa para ejecutar la opción", opcion)
#     print("Termina la ejecución de la opción", opcion)
#     print("Vuelva pronto...\n")
# elif opcion == "5":
#     print("\nHola Tripulantes")
#     print("Programa para ejecutar la opción", opcion)
#     print("Termina la ejecución de la opción", opcion)
#     print("Vuelva pronto...\n")
# elif opcion == "6":
#     print("\nHola Tripulantes")
#     print("Programa para ejecutar la opción", opcion)
#     print("Termina la ejecución de la opción", opcion)
#     print("Vuelva pronto...\n")
# else:
#     print("\nSeleccione una opción válida...\n")