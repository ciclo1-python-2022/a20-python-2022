
# Programa para identificar el pago de entrada al Museo del Oro
# Si es menor 12 años paga $10.000
# Si la edad está entre 12 y 17 años paga $20.000
# Si es adulto mayor, paga $30.000  => a partir de 60 años
# Los demás pagan $60.000   =>   mayor de edad (18) y menor de 60 años
# Si el visitante tiene una estura mayor a metro y medio, incrementa el costo en $3.000
# Si es fin de semana o festivo el costo de la entrada incrementa en 30%
# Visitantes en familia debe indicar la cantidad de integrantes y establecer el costo total a pagar...
# Dividir el código en 2 funciones: la primera función calcular el costo por usuario
# La segunda función que la cantidad de visitantes por familia y un descuento a aplicar
# La segunda función debe llamar a la primera para calcular el costo de cada visitante.
# El museo decide recibir lista de estudiantes/empleados de una institución
# Se debe componer de una lista de diccionarios que contengan los datos de los visitantes (edad y estatura)


# Importar el Módulo
from y_modulo_i_f3__precioEntrMuseo import totalPagar

# ------------------------------------------------------
# Ejecución del Programa...

# Al iniciar el programa
fds = input("\nEs fin de semana o festivo (S/N): ")
if fds == "S" or fds == "s":
    fds = True
else:
    fds = False

cantIntegFlia = int(input("\nDigite la cantidad de integrantes de la familia: "))
descuento = float(input("\nDigite el descuento a aplicar (sin el símbolo porcentaje): "))

precioTotal = totalPagar(cantIntegFlia, descuento, fds)  # Llamado a la función

print("\nEl Precio Total a Pagar por Familia, es de: $", precioTotal, "Pesos\n")