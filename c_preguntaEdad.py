
# Programa para identificar si una persona es mayor de edad.


# Entrada(a)
rta = 0   # defino y declaro la variable rta.
edad = input("Digite su edad: ")
edad = int(edad)

# Transforamción
if edad > 17:
    print("Pasó por el primer if edad > 17")
if edad >= 18:
    rta = "El usuario es mayor de edad."
    print("Pasó por la respuesta verdadera.")
# comentario
else:  # comentario
    rta = "El usuario es menor de edad."
    print("Pasó por la respuesta falsa.")

# Salida(s)
# print("La var rta es:", rta)
# print("La var rta es:")
print(rta)


