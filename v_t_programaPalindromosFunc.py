
# Programa para Identificar Palíndromos
# Palabra o Frase que se lee de igual forma de izquierda a derecha que de derecha a izquierda
# oso - ana - ojo - luz azul - amo la paloma - radar - somos o no somos - Oí lo de mamá: me dolió

# Importar el módulo
from v_t_moduloPalindromosFunc import palindromo


# --------------------------------
# Ejecución del Programa...

frase = ""

while frase.lower().replace(" ", "") != "salir":

    # frase = "Oso"
    frase = input("\nDigite una palabra o frase palíndromo (\"salir\" para terminar): \n")

    if palindromo(frase):
        print("\nLa palabra o frase \"" + frase + "\" es un Palíndromo.\n")
    else:
        print("\nLa palabra o frase \"" + frase + "\" NO es un Palíndromo.\n")


print("\nFin del Programa...\n")
