
# Ejemplos Bucles While y For


# Bucle While

contador = 0
bandera = True
print("\nWhile utilizando bandera contador >= 0 and bandera \n")
# while contador >= 0 and bandera:
# while bandera == True:
while bandera:
    # contador = contador + 1
    # contador += 1
    print("Contador va en: ", contador)
    if contador == 3:
        bandera = False
    contador += 1

print("contador 1 termina en = ", contador)


print("\nWhile utilizando contador <= 3 \n")
contador = 0
while contador < 3:
    # contador = contador + 1
    contador += 1
    print("Contador va en: ", contador)
    # contador += 1

print("contador 2 termina en = ", contador)

print("Salió del while")

print("\nInicio for 1\n")
# for(i = 0; i < 4; i++)
for i in range(4):  # la variable i va de 0 a 3
    print("Variable i = ", i)

print("\nTermina for\n")



print("\nInicio for 2\n")

for i in range(1, 4):  # la variable i va de 1 a 3
    print("Variable i = ", i)

print("\nTermina for\n")



print("\nInicio for 3\n")

for i in range(2, 10 + 1, 2):  # la variable i va de 2 a 10
    print("Variable i = ", i)

print("\nTermina for\n")




print("\nInicio for 4\n")

for i in range(10, 2-1, -2):  # la variable i va de 10 a 2
    print("Variable i = ", i)

print("\nTermina for\n")




print("\nInicio for 5\n")

inicia = 0
termina = 6
incremento = 1
for i in range(inicia, termina, incremento):  # la variable i va de 10 a 2
    print("Variable i = ", i)

print("i termina en = ", i)

print("\nTermina for\n")