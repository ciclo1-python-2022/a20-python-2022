
# Programa para comparar el precio de 2 productos

# Entrada(s)
print("------")

precioProd1 = 5
precioProd2 = 6

# Transformación - Salida(s)
print("------")
print("Inicia if precioProd1 < precioProd2")
if precioProd1 < precioProd2:
    print("La respuesta es True.")
    print("El precio del producto 1 es menor que el precio del producto 2.")
else:
    print("La respuesta es False.")
    print("El precio del producto 1 NO es menor que el precio del producto 2.")
print("Fin if precioProd1 < precioProd2")

print("------")

print("Inicia if precioProd1 > precioProd2")
if precioProd1 > precioProd2:
    # pass
    print("La respuesta es True.")
    print("El precio del producto 1 es mayor que el precio del producto 2.")
else:
    # pass
    print("La respuesta es False.")
    print("El precio del producto 1 NO es mayor que el precio del producto 2.")
print("Fin if precioProd1 > precioProd2")

print("------")

print("Inicia if precioProd1 <= precioProd2")
if precioProd1 <= precioProd2:
    # pass
    print("La respuesta es True.")
    print("El precio del producto 1 es menor o igual que el precio del producto 2.")
else:
    # pass
    print("La respuesta es False.")
    print("El precio del producto 1 NO es menor o igual que el precio del producto 2.")
print("Fin if precioProd1 <= precioProd2")

print("------")

print("Inicia if precioProd1 >= precioProd2")
if precioProd1 >= precioProd2:
    # pass
    print("La respuesta es True.")
    print("El precio del producto 1 es mayor o igual que el precio del producto 2.")
else:
    # pass
    print("La respuesta es False.")
    print("El precio del producto 1 NO es mayor o igual que el precio del producto 2.")
print("Fin if precioProd1 >= precioProd2")

print("------")

print("Inicia if precioProd1 == precioProd2")
if precioProd1 == precioProd2:
    # pass
    print("La respuesta es True.")
    print("El precio del producto 1 es igual al precio del producto 2.")
else:
    # pass
    print("La respuesta es False.")
    print("El precio del producto 1 NO es igual al precio del producto 2.")
print("Fin if precioProd1 == precioProd2")

print("------")

print("Inicia if precioProd1 != precioProd2")
if precioProd1 != precioProd2:
    # pass
    print("La respuesta es True.")
    print("El precio del producto 1 es diferente al precio del producto 2.")
else:
    # pass
    print("La respuesta es False.")
    print("El precio del producto 1 NO es diferente al precio del producto 2.")
print("Fin if precioProd1 != precioProd2")

print("------")