
# ----------------------------------------------
# Listas
# Se declaran con corchetes []
# Son modificables - mutables

ls = [] # lista vacía
ls = list() # lista vacía

ls = [1, 2,3, 4, 5, 6]   # lista homogénea de enteros.
ls = ["1", "2","3", "4", "A", "6"]   # lista homogénea de string.
ls = [1, "A","3", True, 5, 6.6]   # lista heterogénea.
ls = [1, "A","3", True, 5, 6.6, [False, 1, 6.6, True], 10]   # lista heterogénea, nidando otra lista.

lsNomb = ["Hugo", "Paco", "Luis"]
lsTel = [222, 333, 444]

print(f"\nLista de Teléfonos: {lsTel} \n")
lsTel[0] = 111
print(f"Lista de Teléfonos: {lsTel} \n")
lsTel.append(666)  # agregar valores o elementos a la lista
lsTel.append(777)
lsTel.append(888)
lsTel.append(222)
print(f"Lista de Teléfonos: {lsTel} \n")
lsTel.pop(2)
print(f"Lista de Teléfonos: {lsTel} \n")


print(f"\nLista de Nombres: {lsNomb}")
print(f"Lista de Teléfonos: {lsTel} \n")

print(f"El teléfono de {lsNomb[0]}, es: {lsTel[0]}")
print(f"El teléfono de {lsNomb[1]}, es: {lsTel[1]}")
print(f"El teléfono de {lsNomb[2]}, es: {lsTel[2]}\n")

print("\nAnter del For...")
for varIter in range(len(lsNomb)):
    print(f"El valor de varIter {varIter}, entonces lsNomb[{varIter}] = {lsNomb[varIter]}")
print("Después del For...\n")


print("\nAnter del For...")
for varIter in lsNomb:
    print(f"El valor de varIter es: {varIter}, es el elemento de cada iteración en la lista")
print("Después del For...\n")


# ----------------------------------------------
# Tuplas
# Se declaran con paréntesis ()
# Son inmutables, es decir, NO  se pueden modifica después de ser declarados o definidos.

tp = ()  # declarar o definir una tupla vacía.
tp = tuple()   # declarar o definir una tupla vacía.

tp = (1, 2,3, 4, 5, 6)   # tupla homogénea de valores enteros.
tp = ("a", "2", "b", "6") # tupla homogénea de valores tipo string.
tp = ("a", 2, "b", "6", 6.6, True,) # tupla heterogénea
tp = ("a", 2, "b", "6", 6.6, True, (1, "a", 3, True), [3, "x", 6, False, True]) # tupla heterogénea

ls3 = [3, tp, 6.6]
print(ls3)

# Invertir listas o tuplas ... ls[::-1]

# Ejemplo no mutable en consola
# tp[3] = 6  # Genera error => Las tuplas son inmutables

print("\nAnter del For...")
for varIter in range(len(tp)):
    print(f"El valor de varIter {varIter}, entonces tp[{varIter}] = {tp[varIter]}")
print("Después del For...\n")


print("\nAnter del For...")
for varIter in tp:
    print(f"El valor de varIter es: {varIter}, es el elemento de cada iteración en la tupla tp")
print("Después del For...\n")


# ----------------------------------------------
# Diccionarios
# Se declaran con llaves {}
# Son modificables - mutables
# Cada elemento dentro del diccionario se identifica con una llave (key) con su respectivo valor (value)

dc = {}  # declarar o definir un diccionario vacío.
dc = dict()   # declarar o definir un diccionario vacío.

# Ejemplo de un diccionario:
dcTrip1 = {
    "Nombre": "Luis",  # primer elemento (item) del diccionario, tiene como llave (key) Nombre y como valor (value) Luis
    "CC": 98760,   # segundo elemento (item) del diccionario, tiene como llave (key) CC y como valor (value) 98760
    "Tel": 67890
}

print(dcTrip1)

dcTrip2 = {"Nombre": "Paco", "CC": 123456, "Tel": 324516}
print(dcTrip2)

dcTrip1 = {
    "Nombre": "Hugo",  
    "CC": 654321,   
    "Tel": 123456,
    321: 123,
    "": (),
    "": [],
    "": {}
}
print(dcTrip1)


print(f"\nImprime únicamente las claves (keys) del diccionario: {dcTrip1.keys()}")
print(f"Imprime únicamente los valores (values) del diccionario: {dcTrip1.values()}")
print(f"Imprime claves (keys) y valores (values), es decir lo elementos (items): {dcTrip1.items()}\n")



# Pendiente recorrer los diccionarios con el for...
print("\nlongitud del diccionario: ", len(dcTrip1))
# print("\nAnter del For...")
# for varIter in range(len(dcTrip1)):    # No funciona debido a que sus índices son las claves o llaves (keys)
#     print(f"El valor de varIter {varIter}, entonces dcTrip1[{varIter}] = {dcTrip1[varIter]}")
# print("Después del For...\n")


print("\nAnter del For...")
for varIter in dcTrip1:
    print(f"El valor de varIter {varIter}, entonces dcTrip1[{varIter}] = {dcTrip1[varIter]}")
print("Después del For...\n")


print("\nAnter del For...")
for varIter in dcTrip1.keys():
    print(f"El valor de varIter {varIter}, entonces dcTrip1[{varIter}] = {dcTrip1[varIter]}")
print("Después del For...\n")


print("\nAnter del For...")
for varIter in dcTrip1.values():
    print(f"El valor de varIter: {varIter}")
print("Después del For...\n")



print("\nAnter del For...")
for clave, valor in dcTrip1.items():
    print(f"Clave: {clave}, y el Valor: {valor}")
print("Después del For...\n")


dicClaveNum = {
    0: ["a", 3, True],
    1: 321,
    2: "abc",
    3: True,
    4: 6.6,
    5: (3, False),
    6: {"hola": "Tripulantes"}
}

print("\nlongitud del diccionario: ", len(dicClaveNum))
print("\nAnter del For...")
for varIter in range(len(dicClaveNum)):    
    print(f"El valor de varIter {varIter}, entonces dicClaveNum[{varIter}] = {dicClaveNum[varIter]}")
print("Después del For...\n")




# Retorno de 2 o más variables en una función...
def nombFuncion():
    a = 123
    b = ["a", 3, True, 6.6]
    return a, b

x, y = nombFuncion()
print(f"\nx = {x}")
print(f"y = {y}\n")

y, x = nombFuncion()
print(f"\nx = {x}")
print(f"y = {y}\n")

a, b = nombFuncion()
print(f"\na = {a}")
print(f"b = {b}\n")



def nombFuncion():
    a = 123
    b = ["a", 3, True, 6.6]
    c = d
    print(f"\nc = {c}")
    return a, b


d = 6666
b, a = nombFuncion()
print(f"\na = {a}")
print(f"b = {b}\n")




# Conjuntos...
# Archivos json...