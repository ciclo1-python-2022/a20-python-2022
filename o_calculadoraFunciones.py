
# Calculadora Utilizando Funciones

# ------------------- Definir o Declarar la Función o Funciones -------------------------
def suma2Num(parametro1, parametro2):
    resultado = parametro1 + parametro2
    return resultado

def resta2Num(parametro1, parametro2):
    resultado = parametro1 - parametro2
    # if parametro1 > parametro2:
    #     resultado = parametro1 - parametro2
    # else:
    #     resultado = parametro2 - parametro1
    return resultado

def multip2Num(parametro1, parametro2):
    resultado = parametro1 * parametro2
    return resultado

def div2Num(parametro1, parametro2):
    resultado = parametro1 / parametro2
    return resultado



# ------------------- Inicio del Programa -------------------------

# Entrada(S)
a = 5
b = 6

# Transformación
# c = a + b
# c = suma2Num(a, b-a)
c = suma2Num(a, b)
d = resta2Num(a, b)
e = multip2Num(c, d)
f = div2Num(c, d)

# Salida(s)
print("\nc =", a, "+", b, "=", c, "\n")
print("\nd =", a, "-", b, "=", d, "\n")
print("\ne =", c, "x", d, "=", e, "\n")
print("\nf =", c, "/", d, "=", f, "\n")

print(multip2Num(6, 8))
print(multip2Num(suma2Num(3, 4), resta2Num(6, 3)))
# print(multip2Num(suma2Num(float(input("Par1: ")), float(input("Par2: "))), resta2Num(float(input("Par3: ")), float(input("Par4: ")))))

print(float(suma2Num(a, b)))

print("\nLlamado a la función directa 100 + 60 = ", suma2Num(100, 60), "\n")


