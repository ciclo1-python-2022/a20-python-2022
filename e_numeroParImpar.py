
# Programa para identificar si un número es par o impar

# Entrada(s)
# numero = 6
numero = float(input("Digite un número: "))

# # Transformación
# moduloResiduo = numero % 2

# # Salida(s)
# # Si el módulo o residuo es igual a cero entonces el número es par.
# if moduloResiduo == 0:
#     print("Respuesta True")
#     print("El número", numero, "es Par.")
# else:
#     print("Respuesta False")
#     print("El número", numero, "es Impar.")



# ----------------------------------------------------------------
# Otra forma

# # Transformación
# # moduloResiduo = numero % 2
# divEnt = numero // 2
# comprobar = divEnt * 2

# # Salida(s)
# # Si el módulo o residuo es igual a cero entonces el número es par.
# if numero == comprobar:
#     print("Respuesta True")
#     print("El número", numero, "es Par.")
# else:
#     print("Respuesta False")
#     print("El número", numero, "es Impar.")


# ----------------------------------------------------------------
# Otra forma

# # Transformación
# # moduloResiduo = numero % 2
divEnt = int(numero / 2)
comprobar = divEnt * 2

# Salida(s)
# Si el módulo o residuo es igual a cero entonces el número es par.
if numero == comprobar:
    print("Respuesta True")
    print("El número", numero, "es Par.")
else:
    print("Respuesta False")
    print("El número", numero, "es Impar.")



# numero = 6
# if numero % 2 == 0:
#     print("El número", numero, "es Par.")


# numero = 6
# if (numero // 2) * 2 == numero:
#     print("El número", numero, "es Par.")



# numero = 6
# if int(numero / 2) * 2 == numero:
#     print("El número", numero, "es Par.")



# numero = 6
# if (numero / 2) == (numero // 2):
#     print("El número", numero, "es Par.")