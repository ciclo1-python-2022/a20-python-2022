
# Programa para Identificar Palíndromos
# Palabra o Frase que se lee de igual forma de izquierda a derecha que de derecha a izquierda
# oso - ana - ojo - luz azul - amo la paloma - radar - somos o no somos - Oí lo de mamá: me dolió

# --------------------------------
# Declarar o Definir la Función
def palindromo(palabraFrase):
    # palabraFrase = ""
    palabraFrase = palabraFrase.lower()
    palabraFrase = palabraFrase.replace(" ", "")
    palabraFrase = palabraFrase.replace("á", "a")
    palabraFrase = palabraFrase.replace("é", "e")
    palabraFrase = palabraFrase.replace("í", "i")
    palabraFrase = palabraFrase.replace("ó", "o")
    palabraFrase = palabraFrase.replace("ú", "u")
    palabraFrase = palabraFrase.replace(":", "")
    palabraFrase = palabraFrase.replace(".", "")
    palabraFrase = palabraFrase.replace(",", "")
    palabraFrase = palabraFrase.replace(";", "")
    palabraFrase = palabraFrase.replace("-", "")
    palabraFrase = palabraFrase.replace("_", "")
    palabraFrase = palabraFrase.replace("\t", "")
    palFraseInvertida = palabraFrase[::-1]

    if palabraFrase == palFraseInvertida:
        return True   # La palabra / frase es un palíndromo.
    else:
        return False   # La palabra / frase NO es un palíndromo.





# --------------------------------
# Ejecución del Programa...

frase = ""

while frase.lower().replace(" ", "") != "salir":

    # frase = "Oso"
    frase = input("\nDigite una palabra o frase palíndromo (\"salir\" para terminar): \n")

    if palindromo(frase):
        print("\nLa palabra o frase \"" + frase + "\" es un Palíndromo.\n")
    else:
        print("\nLa palabra o frase \"" + frase + "\" NO es un Palíndromo.\n")


print("\nFin del Programa...\n")
