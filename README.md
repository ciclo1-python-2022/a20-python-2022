
Command line instructions

You can also upload existing files from your computer using the instructions below.

Git global setup

git config --global user.name "MisiónTIC UIS_JAS" <br>
git config --global user.email "misiontic.formador17@uis.edu.co" <br>


Create a new repository

git clone https://gitlab.com/ciclo1-python-2022/a20-python-2022.git <br>
cd a20-python-2022 <br>
git switch -c main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push -u origin main <br>


Push an existing folder

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/ciclo1-python-2022/a20-python-2022.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push -u origin main <br>


Push an existing Git repository

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/ciclo1-python-2022/a20-python-2022.git <br>
git push -u origin --all <br>
git push -u origin --tags <br>

