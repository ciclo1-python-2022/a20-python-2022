
# # Símbolo numeral para comentarios

# print("Mi Primer Script en Python")
# print(type("3"))
# type("3")


# print("Editado desde la terminal")

# a = "Hola"
# esp = " "
# b = "Tripulantes"

# print(a + " " + b)
# print(a + esp + b)

# # x = 5
# y = 6
# print(x + y)

# rest = x + y

# print("El resultado de x + y = " + str(rest))
# print("Linea 25 => El resultado de x + y = ", rest, "Impresión separada por coma")




# # Ejecución del código de arriba abajo - izquierda derecha
# # Ejemplo de error q se puede generar por identación
# # Llamados a variables antes de definir


# Programa para sumar 2 números

# Entrada(s)
# x = 5
# x = input("Digite el valor de x: ")
# x = float(x)
x = float(input("Digite el valor de x: "))
# y = 6
# y = input("Digite el valor de y: ")
# y = float(y)
y = float(input("Digite el valor de y: "))

# Transformación
z = x + y

# Salida(s)
print("El resultado de x = ", x, "más", "y =", y, "es: ")
print(z)