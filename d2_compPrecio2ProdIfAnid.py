
# Programa para comparar el precio de 2 productos

# Entrada(s)
print("------")

# precioProd1 = 5
precioProd1 = float(input("Digite el precio del producto 1: "))
# precioProd2 = 6
precioProd2 = float(input("Digite el precio del producto 2: "))

# Transformación - Salida(s)
print("------")
print("Inicia if precioProd1 < precioProd2")
if precioProd1 < precioProd2:
    print("La respuesta es True del primer condicional.")
    print("El precio del producto 1 es menor que el precio del producto 2.")
else:
    print("La respuesta es False del primer condicional.")
    if precioProd1 == precioProd2:
        print("La respuesta es True del segundo condicional.")
        print("El precio del producto 1 igual al precio del producto 2.")
    else:
        print("La respuesta es False del segundo condicional.")
        print("El precio del producto 2 es menor que el precio del producto 1.")

print("Fin if precioProd1 < precioProd2")
print("------")




# if 1 > 2:
#     # pass
#     print("True")
# else:
#     # pass
#     print("False")