
# Programa Comversión o Cambio de Divisas

# Programa para validar Usuario y Contraseña con número máximo de intentos

# --------------------------------
userGuardada = "tripulantes"
contrGaurdada = "abc123"
menu = """
###### Menú de Opciones - Conversor Divisas ######

1. Convertir de Pesos Colombianos a Dólares.
2. Convertir de Dólares a Pesos Colombianos.
3. Registre el valor del dólar respecto al peso colombiano.
4. Mostrar el histórico del dólar, respecto al peso colombiano.

0. Para Terminar o Salir.

Seleccione una opción: """
# --------------------------------


print("\nInicio del Programa...\n")
cont = 1
userPswCheck = False
cantIntentos = 3

# while cont < 3 and userPswCheck == False:
while cont <= cantIntentos and (not userPswCheck):
    # pass
    usuario = input("Digite el usuario: ")
    contrasegna = input("Digite la contraseña: ")

    if usuario == userGuardada and contrasegna == contrGaurdada:
        print("Usuario y Contraseña Validados...")
        userPswCheck = True
    else:
        print("\nUsuario y/o Contraseña Inconrrectos... \n" +
              "Intente Nuevamente... le quedan:", cantIntentos - cont, "intentos.\n")
        cont += 1

print("\nSalió del while\n")

# if userPswCheck == True:
if userPswCheck:
    print("\nBienvenido Ingresó al Conversos de Divisas...")
    # ----------------------------------------------------
    # Programa Completo...
    # menuOpcion = ""
    menuOpcion = str()
    # pesos1Dolar = 0.0
    pesos1Dolar = float()
    # histDolar = ""
    histDolar = str()

    while menuOpcion != "0":
        # pass
        menuOpcion = input(menu)

        if menuOpcion == "1":
            print("\n#############################################################")
            print("\nSeleccionó la opción 1 => Conv. Pesos a Dólares\n")
            if pesos1Dolar != 0:
                # pass
                cantPesos = float(input("Digite la cantidad de Pesos Colombianos que tiene: "))
                cantDolares = cantPesos * 1 / pesos1Dolar
                cantDolares = round(cantDolares, 2)
                print("\nUsted tiene USD$", cantDolares, "dólares.\n")
                continuar = input("\nPresione Enter para continuar....\n")
            else:
                print("\nPor favor, registre el precio del dólar en la ópción 3 del menú.")
                continuar = input("\nPresione Enter para continuar....\n")

            print("\n#############################################################\n")
        elif menuOpcion == "2":
            print("\n#############################################################")
            print("\nSeleccionó la opción 2 => Conv. Dólares a Pesos\n")
            if pesos1Dolar != 0:
                # pass
                cantDolares = float(input("Digite la cantidad de Dólares que tiene: "))
                cantPesos = cantDolares * pesos1Dolar / 1
                cantPesos = round(cantPesos, 2)
                print("\nUsted tiene COP$", cantPesos, "pesos.\n")
                continuar = input("\nPresione Enter para continuar....\n")
            else:
                print("\nPor favor, registre el precio del dólar en la ópción 3 del menú.")
                continuar = input("\nPresione Enter para continuar....\n")

            print("\n#############################################################\n")
        elif menuOpcion == "3":
            print("\n#############################################################")
            print("\nSeleccionó la opción 3 => Registrar Valor del Dolar\n")

            copiaDolar = pesos1Dolar
            pesos1Dolar = input("\nDigite la equivalencia de 1 Dólar en pesos (digite \"salir\" para cancelar): ")
            if pesos1Dolar != "salir" and pesos1Dolar != "SALIR" and pesos1Dolar != "Salir" and pesos1Dolar != "":
                # pass
                histDolar += pesos1Dolar + ", "
                pesos1Dolar = float(pesos1Dolar)
                print("\nSe ha registrado correctamente...")
                continuar = input("\nPresione Enter para continuar....\n")
            else:
                pesos1Dolar = copiaDolar
                print("\nHa decidido cancelar el registro del Dólar...")
                continuar = input("\nPresione Enter para continuar....\n")

            print("\n#############################################################\n")
        elif menuOpcion == "4":
            print("\n#############################################################")
            print("\nSeleccionó la opción 4 => Ver Histórico del Valor del Dolar\n")

            if pesos1Dolar != 0:
                print("\nEl Histórico del Dólar es:")
                print(histDolar)
                continuar = input("\nPresione Enter para continuar....\n")
            else:
                print("\nPor favor, registre el precio del dólar en la ópción 3 del menú.")
                continuar = input("\nPresione Enter para continuar....\n")

            print("\n#############################################################\n")
        elif menuOpcion == "0":
            print("\n#############################################################")
            print("\nSeleccionó la opción 0 => Terminar o Salir del Programa\n")

            menuOpcion = input("Por fovor, confirme que desea salir (S/N): ")
            if menuOpcion == "S" or menuOpcion == "s" or menuOpcion == "":
                menuOpcion = "0"
            print("\n#############################################################\n")
        else:
            print("\n#############################################################")
            print("\nPor Favor, Seleccione una Opción Válida del Menú...\n")
            continuar = input("\nPresione Enter para continuar....\n")
            print("\n#############################################################\n")


    # ----------------------------------------------------
else:
    print("\nHa agotado el número máximo de intentos... \nEspere 1 hora para intentar nuevamente....\n")

print("\nFin del Programa...")
