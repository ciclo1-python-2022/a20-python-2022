
# Módulo de la Función Ejemplo Establecer Valores por Defecto...

# ----------------------------
# Declarar o Definir la Función
# def valor(parametro):
def valor(parametro = 6): # si al llamado de la función no le envían un valor, toma por defecto en este caso el valor de 6.
    print(f"\nEl valor del parámetro es : {parametro}")
    c = parametro * 3
    print(f"Parámetro multiplicado por 3 = {c}")
    return c

    