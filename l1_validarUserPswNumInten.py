
# Programa para validar Usuario y Contraseña con número máximo de intentos

# --------------------------------
userGuardada = "Tripulantes"
contrGaurdada = "Abc123"
# --------------------------------


print("\nInicio del Programa...\n")
cont = 1
userPswCheck = False
cantIntentos = 6

# while cont < 3 and userPswCheck == False:
while cont <= cantIntentos and (not userPswCheck):
    # pass
    usuario = input("Digite el usuario: ")
    contrasegna = input("Digite la contraseña: ")

    if usuario == userGuardada and contrasegna == contrGaurdada:
        print("Usuario y Contraseña Validados...")
        userPswCheck = True
    else:
        print("\nUsuario y/o Contraseña Inconrrectos... \n" +
              "Intente Nuevamente... le quedan:", cantIntentos - cont, "intentos.\n")
        cont += 1

print("\nSalió del while\n")

# if userPswCheck == True:
if userPswCheck:
    print("\nBienvenido ingresó al curso de Python...")
    # ----------------------------------------------------
    # Programa Completo...
    # ----------------------------------------------------
else:
    print("\nHa agotado el número máximo de intentos... \nEspere 1 hora para intentar nuevamente....\n")

print("\nFin del Programa...")
