
# Programa para identificar el pago de entrada al Museo del Oro
# Si es menor 12 años paga $10.000
# Si la edad está entre 12 y 17 años paga $20.000
# Si es adulto mayor, paga $30.000  => a partir de 60 años
# Los demás pagan $60.000   =>   mayor de edad (18) y menor de 60 años
# Si el visitante tiene una estura mayor a metro y medio, incrementa el costo en $3.000
# Si es fin de semana o festivo el costo de la entrada incrementa en 30%
# Visitantes en familia debe indicar la cantidad de integrantes y establecer el costo total a pagar...
# Dividir el código en 2 funciones: la primera función calcular el costo por usuario
# La segunda función que la cantidad de visitantes por familia y un descuento a aplicar
# La segunda función debe llamar a la primera para calcular el costo de cada visitante.
# El museo decide recibir lista de estudiantes/empleados de una institución
# Se debe componer de una lista de diccionarios que contengan los datos de los visitantes (edad y estatura)



    



def totalPagar(cantIntegFlia, descuento, fds):
    precioTotal = 0
    incremento = 30  # valor porcentual a incrementar es decir como tener 30%

    for cont in range(cantIntegFlia):

        # Entrada(s)
        precEntrMuseo = "Error al seleccionar precio..."
        # edad = 6
        texto = "\nDigite la edad del visitante " + str(cont + 1) + " al Museo: "
        edad = int(input(texto))
        texto2 = "Digite la estatura del visitante " + str(cont + 1) + " (m): "
        estatura = float(input(texto2))


        # Transformación
        # llamado a la función precioEntradaMuseo
        precEntrMuseo = precioEntradaMuseo(edad, estatura, fds, incremento)



        # Salida(s)

        print("Precio entrada al museo del integrante: ", (cont + 1), " es: $", precEntrMuseo, "Pesos\n")
        # precioTotal = precioTotal + precEntrMuseo
        precioTotal += precEntrMuseo

    # precioTotal = precioTotal - precioTotal * descuento / 100
    # precioTotal = precioTotal * (1 - descuento / 100)
    precioTotal *= (1 - descuento / 100)

    return precioTotal

def precioEntradaMuseo(edad, estatura, fds, incremento):
    if edad < 12:
        precEntrMuseo = 10000
        # if estatura > 1.50:
        #     precEntrMuseo = precEntrMuseo + 3000
            

    # elif edad >= 12 and edad < 18:
    elif edad < 18:
        precEntrMuseo = 20000
        # if estatura > 1.50:
        #     precEntrMuseo = precEntrMuseo + 3000

    # elif edad >= 18 and edad < 60:
    elif edad < 60:
        precEntrMuseo = 60000
        # if estatura > 1.50:
        #     precEntrMuseo = precEntrMuseo + 3000

    # elif edad >= 60:
    else:
        precEntrMuseo = 30000
        # if estatura > 1.50:
        #     precEntrMuseo = precEntrMuseo + 3000

    # else:
    #     print("Otro caso... digito esdad:", edad)

    if estatura > 1.50:
        # precEntrMuseo = precEntrMuseo + 3000
        precEntrMuseo += 3000 


    # if fds == True:
    if fds:
        # precEntrMuseo = precEntrMuseo + (precEntrMuseo * (30 / 100))
        # precEntrMuseo = precEntrMuseo * (1 + 0.30)
        # precEntrMuseo = precEntrMuseo * (1.30)
        # precEntrMuseo *= 1.30
        # Teniendo en cuenta el valor porcentual en una variable
        # precEntrMuseo = precEntrMuseo + (precEntrMuseo * incremento / 100)
        # precEntrMuseo = precEntrMuseo * (1 + incremento / 100)
        precEntrMuseo *= (1 + incremento / 100)
    
    return precEntrMuseo