
# Programa para identificar el pago de entrada al Museo del Oro
# Si es menor 12 años paga $10.000
# Si la edad está entre 12 y 17 años paga $20.000
# Si es adulto mayor, paga $30.000  => a partir de 60 años
# Los demás pagan $60.000   =>   mayor de edad (18) y menor de 60 años
# Si el visitante tiene una estura mayor a metro y medio, incrementa el costo en $3.000
# Si es fin de semana o festivo el costo de la entrada incrementa en 30%
# Visitantes en familia debe indicar la cantidad de integrantes y establecer el costo total a pagar...
# Dividir el código en 2 funciones: la primera función calcular el costo por usuario
# La segunda función que la cantidad de visitantes por familia y un descuento a aplicar
# La segunda función debe llamar a la primera para calcular el costo de cada visitante.
# El museo decide recibir lista de estudiantes/empleados de una institución
# Se debe componer de una lista de diccionarios que contengan los datos de los visitantes (edad y estatura)


# Importar el Módulo
from y2_modulo_i_f3__precioEntrMuseo import totalPagar
import json

# ------------------------------------------------------
# Ejecución del Programa...

# # Al iniciar el programa
fds = input("\nEs fin de semana o festivo (S/N): ")
if fds == "S" or fds == "s":
    fds = True
else:
    fds = False

# cantIntegFlia = int(input("\nDigite la cantidad de integrantes de la familia: "))
descuento = float(input("\nDigite el descuento a aplicar (sin el símbolo porcentaje): "))

# Extraer a una variable el contenido del archivo .json
with open("/Users/jas/Ciclo1-Python/A20/y2_archJson_i_f3__precioEntrMuseo.json") as archivo:
    dictEntidad = json.load(archivo)

print("\nContenido de la variable dictEntidad:")
print(dictEntidad, "\n")
print(f"Longitud diccionario: {len(dictEntidad)}", "\n")

for i, j in dictEntidad.items():
    print(f"La clave/llave: {i}, contiene: {j}\n")

print('dictEntidad["Integrantes"]:')
print(dictEntidad["Integrantes"], "\n")

listaVisitantes = dictEntidad["Integrantes"]
print("listaVisitantes: ")
print(listaVisitantes, "\n")

# for k in dictEntidad["Integrantes"]:
for k in listaVisitantes:
    print(f"k: {k}")
    print(f'k["Nombre"]: {k["Nombre"]}')
    print(f'k["Cod"]: {k["Cod"]}')
    print(f'k["Edad"]: {k["Edad"]}')
    print(f'k["Estatura"]: {k["Estatura"]}\n')



# precioTotal = totalPagar(cantIntegFlia, descuento, fds)  # Llamado a la función
# precioTotal = totalPagar(dictEntidad["Integrantes"], descuento, fds)  # Llamado a la función
precioTotal = totalPagar(listaVisitantes, descuento, fds)  # Llamado a la función

print(f"\nEl Precio Total a Pagar por Familia con el {descuento}% de descuento,\nEs de: $", precioTotal, "Pesos\n")


empresa = {
    "NombreEmpresa": dictEntidad["NombreEntidad"],
    "CantidadEmpleados": len(dictEntidad["Integrantes"]),
    "Descuento": descuento,
    "fds": fds,
    "TotalPagar": precioTotal
}

nombArch = input("Digite el nombre del archivo a guardar: ")
# cantEmp = input("Digite la cantidad de Empleados: ")

# with open("y2_archivoEmpresa.json", "w") as archivo:
with open(f"/Users/jas/Ciclo1-Python/A20/{nombArch}.json", "w") as archivo:
    json.dump(empresa, archivo, indent=4)