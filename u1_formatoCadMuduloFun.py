
# -----------------------------------------------
# importar todo sin utilizar el punto de un módulo.
# import math
# print("\nmath.pi =", math.pi)
# print("math.e =", math.e, "\n")

from math import *
print("\npi = ", pi)
print("e = ", e, "\n")


# -----------------------------------------------
# Formato para Cadenas de Caracteres
a = "Tripulante"
b = 123
c = f"Hola {a}, su código de identificación es: {b}"
print(f"\n{c}\n")


a2 = "Trip"
b2 = 321
c2 = "Hola {0}, su código de identificación es: {1}".format(a2, b2)
print("\n{}\n".format(c2))

c2 = "Hola {1}, su código de identificación es: {0}".format(a2, b2)
print("\n{}\n".format(c2))

z = "JAS"
w = "\nHola {0}\n"
print(w.format(z))


# -----------------------------------------------
# Funciones con Valor por defecto

# ----------------------------
# Declarar o Definir la Función
# def valor(parametro):
def valor(parametro = 6): # si al llamado de la función no le envían un valor, toma por defecto en este caso el valor de 6.
    print(f"\nEl valor del parámetro es : {parametro}")
    c = parametro * 3
    print(f"Parámetro multiplicado por 3 = {c}")
    return c

# ----------------------------
# Ejecutar el Programa...
print(f"Retorno de la función: {valor()}\n")
print(f"Retorno de la función: {valor(10)}\n")
c = "Tripulantes "
print(f"Retorno de la función: {valor(c)}\n")




# -----------------------------------------------
# Generar un módulo con una función. => Se hace en archivos a parte...